import { Suspense, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AboutComponent from "./component/AboutComponent";
import { Datas, DatasAbout } from "./component/data/Datas";
import ErrorComponent from "./component/ErrorComponent";
import FaceBookLoginComponent from "./component/FaceBookLoginComponent";
import Footer from "./component/Footer";
import HomeCoponent from "./component/HomeCoponent";
import Navigation from "./component/Navigation";
import VersionComponent from "./component/VersionComponent";

function App() {
  const [data, setData] = useState([...Datas]);
  const [dataAbout, setDataAbout] = useState([1, 2, 3]);
  return (
    <Suspense fallback = "loading">
      <BrowserRouter>
        <Navigation />
        <Routes>
          <Route path="/" element={<HomeCoponent data={data} />} />
          <Route
            path="/about"
            element={<AboutComponent dataAbout={dataAbout} />}
          />
          <Route path="/version" element={<VersionComponent />} />
          <Route path="*" element={<ErrorComponent />} />
          <Route path="/login" element={<FaceBookLoginComponent/>} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
