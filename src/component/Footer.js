import React from "react";
import { Card, Container, Row } from "react-bootstrap";
import IMG from "../images/logoHRD.png";

function Footer() {
  return (
    <>
      <Container style={{ marginTop: "50px" }}>
        <Row>
          <div className="col-lg-4" style={{ textAlign: "center" }}>
            <Card.Img
              className=" "
              variant="top"
              src={IMG}
              style={{ width: "150px", height: "150px" }}
            />
            <p>
              <b>&copy; រក្សាសិទ្ធិគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ២០២២</b>
            </p>
          </div>
          <div
            className="col-lg-4"
            style={{ textAlign: "center", paddingTop: "20px" }}
          >
            <h3>Address</h3>
            <p>
              <span>
                <b>Address:</b>
              </span>{" "}
              #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Phnom Penh,
              Cambodia.
            </p>
          </div>
          <div
            className="col-lg-4"
            style={{ textAlign: "center", paddingTop: "20px" }}
          >
            <h3>Contact</h3>
            <p>
              <span>
                <b>Tel: </b>
              </span>
              012 998 919 (Khmer)
            </p>
            <p>
              <span>
                <b>Tel: </b>
              </span>
              085 402 605 (Korean)
            </p>
            <p>
              <span>
                <b>Email: </b>
              </span>
              info.kshrd@gmail.com phirum.gm@gmail.com
            </p>
          </div>
        </Row>
      </Container>
    </>
  );
}

export default Footer;
