import React from "react";
import { Container, Image, Row, Col } from "react-bootstrap";

function AboutComponent({ dataAbout }) {
  return (
    <>
      <Container className="my-2">
        <Row>
          {dataAbout?.map((item, i) => (
            <div key={i} style={{ display: "flex", margin: 5 }}>
              <Image
                className={i % 2 === 0 ? "order-1" : "order-2"}
                src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
                width="50%"
                height="100%"
                style={{ flex: 1 }}
              />
              <div
                className={
                  i % 2 === 0
                    ? "mx-3 bg-light order-2"
                    : "mx-3 bg-light order-1"
                }
                style={{ flex: 1, height: "100%" }}
              >
                <p className="p-3">
                  Korea Software HRD Center is an academy training center for
                  training software professionals in cooperation with Korea
                  International Cooperation Agency(KOICA) and Webcash in April,
                  2013 in Phnom Penh, Cambodia. <br />
                  <br /> From 2020, Korea Software HRD Center has been become
                  Global NGO with the name Foundation for Korea Software Global
                  Aid (KSGA), main sponsored by Webcash Group, to continue
                  mission for ICT Development in Cambodia and will recruit 60 t0
                  80 scholarship students every year.
                </p>
              </div>
            </div>
          ))}
        </Row>
      </Container>
    </>
  );
}

export default AboutComponent;
