import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useTranslation } from 'react-i18next';
function HomeCoponent({ data }) {
  const { t, i18n } = useTranslation();
  return (
    <>
      <Container className="my-3">
        <Row>
          {data?.map((item,i) => (
            <Col key={i}>
              <Card style={{ width: "18rem" }}>
                <Card.Img variant="top" src={item.thumbnail} />
                <Card.Body className="text-center">
                  <Card.Title className="">{item.title}</Card.Title>
                  <Card.Text>{item.description}</Card.Text>
                  <Card.Title className="p-1 fs-3 text-danger">
                    {item.price} $
                  </Card.Title>
                  <Button variant="success">Buy the course</Button>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

export default HomeCoponent;
