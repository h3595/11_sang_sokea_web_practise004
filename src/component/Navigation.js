import React, { useEffect, useState } from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "./style/styles.css";
import { useTranslation } from "react-i18next";

function Navigation() {
  const [langCode, setLangCode] = useState("en");
  const { t, i18n } = useTranslation();
  const language = ["English", "ភាសាខ្មែរ"];
  const [lg, setLg] = useState(false);

  const onToggleLanguage = () => {
    setLg(!lg);
    setLangCode(lg ? "kh" : "en");
  };

  useEffect(() => {
    i18n.changeLanguage(langCode);
  }, [lg]);
  return (
    <>
      <Navbar className="bg-dark">
        <Container>
          <Navbar.Brand className="text-light" href="#">
            PT-004
          </Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Nav.Link className="my-navbar text-light" as={NavLink} to="/">
              {t("home")}
            </Nav.Link>
            <Nav.Link className="my-navbar text-light" as={NavLink} to="/about">
              About
            </Nav.Link>
            <Nav.Link
              className="my-navbar text-light"
              as={NavLink}
              to="/version"
            >
              Our Version
            </Nav.Link>

            <Nav.Link className="my-navbar text-light" as={NavLink} to="/login">
              Login
            </Nav.Link>

            <Nav.Link
              className="my-navbar text-light"
              onClick={onToggleLanguage}
            >
              {lg ? language[1] : language[0]}
            </Nav.Link>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Navigation;
