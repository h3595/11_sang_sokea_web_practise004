import React from "react";
import FacebookLogin from "react-facebook-login";

export default function FaceBookLoginComponent() {
  const responseFacebook = (response) => {
    console.log(response);
  };
  return (
    <div>
      <FacebookLogin
        appId="991387091577245"
        autoLoad={true}
        fields="name,email,picture"
        onClick={responseFacebook}
        callback={responseFacebook}
      />
    </div>
  );
}
